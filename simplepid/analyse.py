#!/usr/bin/env python
# -*- coding: utf-8 -*-

import csv
import matplotlib.pyplot as plt

def main():
  setpoint = []
  pwm = []
  output = []
  eqep = []
  d_f = []
  p_f = []
  int_err = []
  last_output = []
  delta_output = []
  speed = []
  current = []  
  accel = []

  with open("output", "rb") as csv_file:
    csv_reader = csv.reader(csv_file)
    for row in csv_reader:
      if len(row):
        setpoint.append(row[0])
        #pwm.append(row[1])
        #output.append(row[1])
        eqep.append(int(row[2]))
        #p_f.append(float(row[3])/10000)
        #int_err.append(float(row[4])/10000)
        #d_f.append(float(row[5])/10000)
        #delta_output.append(float(row[1]) - float(row[6]))
        current.append(int(row[6]))
        if len(eqep) > 1:
          speed.append(eqep[-1] - eqep[-2])
          accel.append(speed[-1] - speed[-2]) 
        else:
          speed.append(0)
          accel.append(0)       
  
  plt.plot(setpoint, 'b')
  #plt.plot(pwm, 'g')
  plt.plot(eqep, 'r')
  #plt.plot(d_f, 'y')
  #plt.plot(p_f, 'k')
  #plt.plot(int_err, 'c')
  #plt.plot(delta_output, 'g')
  plt.plot(current, 'y')
  plt.plot(speed, 'k')
  plt.plot(accel, 'g')
  plt.show()
  return 0

if __name__ == '__main__':
	main()

