/*
* simplepid.c
*
* pru code for pru 0 in a simple pid example
* mainly taken from ti example
*/

#include <stdint.h>
#include <limits.h>
#include <pru_cfg.h>
#include <pru_ecap.h>
#include <sys_pwmss.h>

/* shared pid structure */
struct pid_data {
    int Kp_f, Ki_f, Kd_f;

    int setpoint;
    int p_f;
    int int_err;
    int d_f;
    int input,output, last_output;
    int min_output, max_output;
    int current;
};

volatile register uint32_t __R30;
volatile register uint32_t __R31;

/* Memory mapping */
volatile near struct pid_data pid __attribute__((cregister("PRU_DMEM_0_1", near), peripheral));

/* interupts */
#define PRU0_ARM_EVT        (19)
#define PRU0_ARM_TRIGGER    (__R31 = PRU0_ARM_EVT - 16 | (1 << 5)

/* Non-CT register defines */
#define CM_PER_EPWMSS1 	(*((volatile unsigned int *)0x44E000CC))
#define FIFO0DATA		(*((volatile unsigned int *)0x44E0D100))
#define	CTRL			(*((volatile unsigned int *)0x44E0D040))
#define STEPENABLE		(*((volatile unsigned int *)0x44E0D054))
#define STEPCONIFIG1	(*((volatile unsigned int *)0x44E0D064))
#define	ADC_CLKDIV		(*((volatile unsigned int *)0x44E0D04C))

/* PWM generation definitions*/
#define PERIOD_CYCLES       0x1000

/* 'Float' conversion */
#define SHIFT    0x0E

/* function prototypes */
void init_pwm();
void init_eqep();
void update_pid(volatile struct pid_data* pid);
void init_pid(volatile struct pid_data* pid);
void init_adc();

/*--------------- Main ----------------------*/

int main(void)
{
	/* allow OCP master port access by the PRU so the PRU can read external memories */
	CT_CFG.SYSCFG_bit.STANDBY_INIT = 0;
	/* Configure GPI and GPO as Mode 0 (Direct Connect) */
	CT_CFG.GPCFG0 = 0x0000;

	init_pwm();
	init_eqep();
	init_pid(&pid);
	init_adc();

	while(1){
		CT_ECAP.CAP2_bit.CAP2 = pid.output;
		pid.input = PWMSS1.EQEP_QPOSCNT;
		pid.current = FIFO0DATA;
		update_pid(&pid);
	}
	return 0;
}

/*
 * Init PWM sub-system
 */
void init_pwm() {
    /* Set PWM memory to 0 */
	//data.pwm_out = 0;

    /* Enable APWM mode and enable asynchronous operation; set polarity to active high */
    CT_ECAP.ECCTL2 = 0x02C0;

    /* Set number of clk cycles in a PWM period (APRD) */
    CT_ECAP.CAP1 = PERIOD_CYCLES;

    /* Enable ECAP PWM Freerun counter */
    CT_ECAP.ECCTL2 |= 0x0010;
}

/*
 * Init eQEP reader
 */
void init_eqep() {
    /* Set RPM memory to 0 */
    //data.enc_pos = 0;

    /* Enable PWMSS1 clock signal generation */
    //while (!(CM_PER_EPWMSS1 & 0x2))
    //    CM_PER_EPWMSS1 |= 0x2;

    /* Set to defaults in quadriture mode */
    PWMSS1.EQEP_QDECCTL = 0x00;

    /* Enable unit timer
     * Enable capture latch on unit time out
     * Enable quadrature position counter
     * Enable software loading of position counter
     * Reset position counter on unit time event to gauge RPM
     */
    //PWMSS1.EQEP_QEPCTL = 0x308E;

    /* Set prescalers for EQEP Capture timer and UPEVNT */
    /* Note: EQEP Capture unit must be disabled before changing prescalar */
    //PWMSS1.EQEP_QCAPCTL = 0x0073;

    /* Enable EQEP Capture */
    PWMSS1.EQEP_QCAPCTL |= 0x8000;

    /* Enable unit time out interupt */
    //PWMSS1.EQEP_QEINT |= 0x0800;

    /* Clear encoder count */
    PWMSS1.EQEP_QPOSCNT_bit.QPOSCNT = 0x00000000;

    /* Set max encoder count */
    PWMSS1.EQEP_QPOSMAX_bit.QPOSMAX = UINT_MAX;

    /* Clear timer */
    //PWMSS1.EQEP_QUTMR_bit.QUTMR = 0x00000000;

    /* Set unit timer period count */
    /*  QUPRD = Period * 100MHz */
    //PWMSS1.EQEP_QUPRD_bit.QUPRD = 0x007FFFFF; // (~1/12s) @ 100MHz

    /* Clear all interrupt bits */
    PWMSS1.EQEP_QCLR = 0xFFFF;
}

/*
 * Init analogue input, for current sense
 */
void init_adc()
{
	/*set sampling rate*/
	//ADC_CLKDIV = 0;

	/*Enable ADC */
	CTRL |= 0x05;

	/*Enable step 1 */
	STEPENABLE |= 0x02;

	/*Configure step 1 to continuous mode*/
	STEPCONIFIG1 |= 0x11;
}

/*
 * Init PID values
 */
void init_pid(volatile struct pid_data* pid) {
    pid->input = 0;
    pid->output = 2048;
    pid->last_output = 0;

    pid->setpoint = 0;
    pid->int_err = 0;
    //pid->last_d_f = 0;
}

/*
 * PID calculations
 */
void update_pid(volatile struct pid_data* pid) {
    int p_f, d_f;
    int output_f, output;
    //int temp_i, temp_o;

    /* Calculate error */
    int error = (pid->setpoint - pid->input);

    /* Calculate P term */
    pid->p_f = p_f = pid->Kp_f * error;

    /* Integrate I term */
    pid->int_err += (pid->Ki_f * error) >> SHIFT;
/*
    if ((temp_i = pid->Ki_f * error) < 0){
    	pid->int_err += -1 * ((0 - temp_i) >> SHIFT);
    }
    else
    	pid->int_err += temp_i >> SHIFT;
*/
    /* Calculate D term */
    //pid->d_f = d_f = pid->Kd_f * (pid->output - pid->last_output);
    		//+ pid->last_d_f);
    //pid->d_f = d_f = pid->Kd_f * (error - pid->last_error);
    pid->d_f = d_f = (pid->Kd_f * pid->output) - (pid->Kd_f * pid->last_output);
    //pid->last_d_f = pid->d_f;

    /* Sum PID output */
    output_f = p_f + pid->int_err + d_f;
    output = output_f >> SHIFT;
/*
    if ((output_f = p_f + pid->int_err + d_f) < 0){
    	output = -1 * ((0 - output_f) >> SHIFT);
    }
    else
        output = output_f >> SHIFT;
*/

    /* Set output_f, check min/max output */
    if (output < pid->min_output) output = pid->min_output;
    if (output > pid->max_output) output = pid->max_output;

    //if (output < 0 && output > -15) output = -15;
    //if (output > 0 && output < 15) output = 15;

    //pid->last_error = error;
    pid->last_output = pid->output;
    //pid->output = pid->max_output - output;
    pid->output = 2048 + output;
}
