
// setup the pwm 
// echo am33xx_pwm > /sys/devices/bone_capemgr.9/slots
// echo bone_pwm_P9_14 > /sys/devices/bone_capemgr.9/slots
// cd /sys/devices/ocp.3/pwm_test_P9_14.12
// echo 1 > run

// echo 10000 > period 
// echo 5000 > duty 
// results in TBPRD=1000, CMPA=500

// make the eqep work
// echo bone_eqep1 > /sys/devices/bone_capemgr*/slots

// P8.33 encoderA, P8.35 encoderB, P9.14 PWM output

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <unistd.h>
#include <time.h>

int main(int argc, char **argv)
{
    long PWMSS_offset = 0x48302000; 
    long PWMSS_size = 0x260; 
    struct timeval start_time, end_time; 

    long TBCTL = 0x200 + 0x00; 
    long CMPCTL = 0x200 + 0x0E; 
    long CMPA = 0x200 + 0x12; 
    long TBCNT = 0x200 + 0x08; 
    long TBPRD = 0x200 + 0x0A; 
    long AQCTLA = 0x200 + 0x16; 

    long QPOSCNT = 0x180 + 0x00; 
    long QCAPCTL = 0x180 + 0x2C; 


    long i = 0; 
    long j = 0; 
    int cntstart0 = 0; 
    unsigned short cmpaval = 0; 
    int fd; 
    FILE* fout; 
    void* pwmssaddr; 
    unsigned short *cmpa_reg; 
    unsigned short *tbprd_reg; 
    unsigned int* qcapctl_reg; 
    int* qposcnt_reg; 
    long double sysTime; 
    unsigned long usec; 
    long usecs[1000]; 
    long cntls[1000]; 
    
    printf("hi there\n\n");
    fd = open("/dev/mem", O_RDWR);
    pwmssaddr = mmap(0, PWMSS_size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, PWMSS_offset);

    cmpa_reg = pwmssaddr + CMPA;
    tbprd_reg = pwmssaddr + TBPRD;
    qcapctl_reg = pwmssaddr + QCAPCTL; 
    qposcnt_reg = pwmssaddr + QPOSCNT; 
    close(fd); 

    gettimeofday( &start_time, 0); 
    fout = fopen("motorcurves.txt",  "w");

    *qcapctl_reg = 0x8000; 
    
    for (i = 2; i < 32; i++) {
        *cmpa_reg = 500; 
        usleep(1000*1000); 
        
        cntstart0 = *qposcnt_reg; 
        cmpaval = (i/2)*25*((i&1)==0?1:-1)+500; 
        fprintf(fout, "\n%d\n", cmpaval); 
        gettimeofday(&start_time, 0); 
        usec = start_time.tv_usec;
        for (j = 0; j < 600; j++) {
            if (j == 10)
                *cmpa_reg = cmpaval; 
            if (j == 200)  // quickflip
                *cmpa_reg = 1000-cmpaval; 
            if (j == 400)
                *cmpa_reg = 500; 
            gettimeofday(&start_time, 0); 
            usecs[j] = start_time.tv_usec - usec; 
            cntls[j] = (*qposcnt_reg - cntstart0); 
            //fprintf(fout, " %d %d %d\n", i, start_time.tv_usec - usec, (*qposcnt_reg - cntstart0)); 
            usleep(1*400); 
        }

        for (j = 0; j < 600; j++) {
            fprintf(fout, " %d %d %d\n", i, usecs[j], cntls[j]); 
        }
        
        fprintf(stderr, "cmpa %d %d %d %d\n", i, cmpaval, *qposcnt_reg - cntstart0, start_time.tv_usec);
    }
    *cmpa_reg = 500; 
}


