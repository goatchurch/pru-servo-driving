#!/usr/bin/env python
"""
Pru motor synth controller.
"""

# uses python-midi module :
# git clone https://github.com/vishnubob/python-midi.git
# needs swig package to install:
# apt-get install swig
# then:
# python setup.py install

# cape:
# echo BB-BONE-PRUCAPE > /sys/devices/bone_capemgr.9/slots

# pin:
# P9_31

import sys
import time
import midi
import midi.sequencer as sequencer
from midi.events import NoteOnEvent, NoteOffEvent
from math import pow
import pypruss
from mmap import mmap
import struct

#--------PRU stuff:---------------#

pypruss.modprobe()
pypruss.init()
pypruss.open(0)
pypruss.pruintc_init()
pypruss.pru_write_memory(0, 0, [1000])
pypruss.exec_program(0, "./pwm.bin")

#-------MIDI stuff:---------------#

# vmpk will be 128 0 when run for the first time
# set this as command line options with midiplay for playing files 
client = 128
port = 0

seq = sequencer.SequencerRead(sequencer_resolution=120)
seq.subscribe_port(client, port)
seq.start_sequencer()

#channel = 12
channel = 0

#-----Eqep stuff------------------#

PWMSS_offset = 0x48302000
PWMSS_size = 0x4830225F - PWMSS_offset + 1
QPOSCNT = 0x180 + 0x00
QCAPCTL = 0x180 + 0x2C

fp = open("/dev/mem", "r+b")
memp = mmap(fp.fileno(), PWMSS_size, offset=PWMSS_offset)            

memp[QCAPCTL:QCAPCTL+4] = struct.pack("<L", 0x8000)
position = struct.unpack("<L", memp[QPOSCNT:QPOSCNT+4])[0]
drift = 0

#------Main loop------------------#            
while True:
  event = seq.event_read()
  if event is not None and event.channel == channel:
      print event
      if isinstance(event, NoteOnEvent):
        #------note to freq. formula:-------#
        # fn = fo * a^n
        # where:
        # fo = 440Hz (A5, midi 69)
        # n = steps from A5
        # a = 2^1/12
        freq = 440 * pow(1.059463094359,  event.data[0] - 69)
        pru_loops = int(50000000/freq)
        print freq, pru_loops
        pypruss.pru_write_memory(0, 0, [pru_loops])
      elif isinstance(event, NoteOffEvent):
        pypruss.pru_write_memory(0, 0, [1000])
  
  # read eqep to check for drifting, not tested yet:    
  #drift = position - struct.unpack("<L", memp[QPOSCNT:QPOSCNT+4])[0]