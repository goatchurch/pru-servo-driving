
/* user space side program for quadrature reader*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <prussdrv.h>
#include <pruss_intc_mapping.h>

#define USE_BOTH 1
#define BIN_0 "quad_0.bin"
#define BIN_1 "quad.bin"
#define GLB_BUF 0x00002FA8

/* function defs */
static int pru_cleanup(void);

/* Shared PID data structure - ensure both shared stucts match both PRUs */
struct pid_data {
    /* PID tunings */
    int Kp_f, Ki_f, Kd_f;

    /* PID controls */
    int setpoint;
    int int_err;
    int input, output, last_output;
    int min_output, max_output;
};

/* Shared memory block struct */
struct shared_mem {
    volatile char init_flag;
    volatile unsigned int pwm_out;
    volatile int enc_rpm;
    volatile struct pid_data pid;
};

/* Output struct */
struct output_s {
	volatile unsigned int pwm;
	volatile int quad;
};

int main(int argc, char **argv)
{
    static void *pruDataMem;                      
    //static struct shared_mem *pruDataMem_struct;  //pointer to PRU memory
    static struct output_s *pruDataMem_struct;  //pointer to PRU memory
    //static unsigned int *pruDataMem_int;
    //time_t rawtime, oldtime;
    
    /* load interupt table */ 
    tpruss_intc_initdata intc = PRUSS_INTC_INITDATA;
    
    /* Check for sudo */   
    if(geteuid()) {
      fprintf(stderr, "%s must be run as root to use prussdrv\n", argv[0]);
      return -1;
    }
   
    /* initialize PRUSSDRV */
    if((prussdrv_init()) != 0) {
      fprintf(stderr, "prussdrv_init() failed\n");
      pru_cleanup();
      return -1;
    }
   
    /* open the interrupt */
    if((prussdrv_open(PRU_EVTOUT_0)) != 0) {
      fprintf(stderr, "prussdrv_open() failed\n");
      pru_cleanup();
      return -1;
    }
   
    /* initialize interrupt */
    if((prussdrv_pruintc_init(&intc)) != 0) {
      fprintf(stderr, "prussdrv_pruintc_init() failed\n");
      pru_cleanup();
      return -1;
    }
    
    /* get pru memory pointer*/
    prussdrv_map_prumem (PRUSS0_PRU0_DATARAM, &pruDataMem);
    //pruDataMem += GLB_BUF;
    //pruDataMem_struct = (struct shared_mem*) pruDataMem;
    pruDataMem_struct = (struct output_s*) pruDataMem;
    //pruDataMem_int = (unsigned int*) pruDataMem;

    //printf("%p\n", pruDataMem_struct);
    
    /* load and run the first PRU program */
    if((prussdrv_exec_program(0, BIN_0)) < 0) {
      fprintf(stderr, "prussdrv_exec_program() failed\n");
      pru_cleanup();
      return -1;
    }
   
    /* load and run the second PRU program */
    if (USE_BOTH){
      if((prussdrv_exec_program(1, BIN_1)) < 0) {
        fprintf(stderr, "prussdrv_exec_program() failed\n");
        pru_cleanup();
        return -1;
      }
    }
    
    /* notify PRU(s) have loaded */
    printf("PRUSS running...\n");
    
    /* wait for pru to notify of good reading */
    //prussdrv_pru_wait_event(PRU_EVTOUT_0);

    //printf("Quad read: %d\n", pruDataMem_struct->pid.input);
    
    //rawtime = oldtime = time(NULL);
    while(1){
        prussdrv_pru_wait_event (PRU_EVTOUT_0);
        //rawtime = time(NULL);
        //if (rawtime > oldtime){
            //printf("Quad read: %d\n", *pruDataMem_int);
            printf("%d\t\t%d\n", pruDataMem_struct->quad, pruDataMem_struct->pwm );
            //printf("Quad read: %#010x\n", *pruDataMem_int);
            //printf("%d\n", pruDataMem_struct->pid.max_output);
            //oldtime = rawtime;
        //} 
        prussdrv_pru_clear_event (PRU_EVTOUT_0, PRU0_ARM_INTERRUPT);
    }
    
    /* clear the event, disable the PRU and let the library clean up */
    if(prussdrv_pru_clear_event(PRU_EVTOUT_0, PRU0_ARM_INTERRUPT)) {
      fprintf(stderr, "prussdrv_pru_clear_event() failed\n");
      return -1;
    }
    return pru_cleanup();
}

/*** pru_cleanup() -- halt PRU and release driver
Performs all necessary de-initialization tasks for the prussdrv library.
Returns 0 on success, non-0 on error.
***/
static int pru_cleanup(void) {
   int rtn = 0;

   /* halt and disable the PRU (if running) */
   if((rtn = prussdrv_pru_disable(0)) != 0) {
      fprintf(stderr, "prussdrv_pru0_disable() failed\n");
      rtn = -1;
   }
   if (USE_BOTH){
      if((rtn = prussdrv_pru_disable(1)) != 0) {
         fprintf(stderr, "prussdrv_pru1_disable() failed\n");
         rtn = -1;
      }
   }
   /* release the PRU clocks and disable prussdrv module */
   if((rtn = prussdrv_exit()) != 0) {
      fprintf(stderr, "prussdrv_exit() failed\n");
      rtn = -1;
   }

   return rtn;
}