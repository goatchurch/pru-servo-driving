/*
* Copyright (C) 2013 Pantelis Antoniou <panto@antoniou-consulting.com>
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*/

// BB-BONE-PRU-O4 changed to get prucape going
// pins 8_20, 8_21 removed due to emmc clash


/dts-v1/;
/plugin/;

/ {
	compatible =  "ti,beaglebone",
                "ti,beaglebone-black",
                "ti,beaglebone-green";

	/* identification */
	part-number = "BB-BONE-PRUCAPE";
	version = "00A0";

	/* state the resources this cape uses */
	exclusive-use =
		/* the pin header uses */
		"P9.27",	/* pru0: pr1_pru0_pru_r30_5 */

		"P8.11",	/* pru0: pr1_pru0_pru_r30_15 */
		"P8.12",	/* pru0: pr1_pru0_pru_r30_14 */
		"P9.25",	/* pru0: pr1_pru0_pru_r30_7 */
		"P9.41",	/* pru0: pr1_pru0_pru_r30_6 */
		"P9.42",	/* pru0: pr1_pru0_pru_r30_4 */
		"P9.28",	/* pru0: pr1_pru0_pru_r30_3 */
		"P9.30",	/* pru0: pr1_pru0_pru_r30_2 */
		"P9.29",	/* pru0: pr1_pru0_pru_r30_1 */
		"P9.31",	/* pru0: pr1_pru0_pru_r30_0 */
				/* pru0: pr1_pru0_pru_r30_13 is on MMC0_CMD */
				/* pru0: pr1_pru0_pru_r30_12 is on MMC0_CLK */
				/* pru0: pr1_pru0_pru_r30_11 is on MMC0_DAT0 */
				/* pru0: pr1_pru0_pru_r30_10 is on MMC0_DAT1 */
				/* pru0: pr1_pru0_pru_r30_9 is on MMC0_DAT2 */
				/* pru0: pr1_pru0_pru_r30_8 is on MMC0_DAT3 */

//		"P8.20",	/* pru1: pr1_pru1_pru_r30_13 */
//		"P8.21",	/* pru1: pr1_pru1_pru_r30_12 */
//    removed due to emmc clash

		"P8.27",	/* pru1: pr1_pru1_pru_r30_8 */
		"P8.28",	/* pru1: pr1_pru1_pru_r30_10 */
		"P8.29",	/* pru1: pr1_pru1_pru_r30_9 */
		"P8.30",	/* pru1: pr1_pru1_pru_r30_11 */
		"P8.39",	/* pru1: pr1_pru1_pru_r30_6 */
		"P8.40",	/* pru1: pr1_pru1_pru_r30_7 */
		"P8.41",	/* pru1: pr1_pru1_pru_r30_4 */
		"P8.42",	/* pru1: pr1_pru1_pru_r30_5 */
		"P8.43",	/* pru1: pr1_pru1_pru_r30_2 */
		"P8.44",	/* pru1: pr1_pru1_pru_r30_3 */
		"P8.45",	/* pru1: pr1_pru1_pru_r30_0 */
		"P8.46",	/* pru1: pr1_pru1_pru_r30_1 */
				/* pru1: pr1_pru1_pru_r30_14 is on UART0_RXD */
				/* pru1: pr1_pru1_pru_r30_15 is on UART0_TXD */
				
		"P8.33",	/*encoder inputs*/
		"P8.35",
		"P9.14",	/*ehrpwm1A*/
				
		/* the hardware IP uses */
		"pru0",
		"pru1",
		"epwmss1",
		"eqep1",
		"ehrpwm1A",
		"ehrpwm1";

	fragment@0 {
		target = <&am33xx_pinmux>;
		__overlay__ {

			pru_gpio_pins: pinmux_pru_gpio_pins {
				pinctrl-single,pins = <
					0x1a4 0x0f 	/* P9 27 GPIO3_19: mcasp0_fsr.gpio3[19] | MODE7 | OUTPUT */
				>;
			};

			pru_pru_pins: pinmux_pru_pru_pins {
				pinctrl-single,pins = <
					0x1a4 0x2E	/* mcasp0_fsr.pr1_pru0_pru_r30_5, MODE6 | INPUT | PRU */
					0x034 0x26	/* gpmc_ad13.pr1_pru0_pru_r30_15, MODE6 | OUTPUT | PRU */
					0x030 0x26	/* gpmc_ad12.pr1_pru0_pru_r30_14, MODE6 | OUTPUT | PRU */
					
					0x1ac 0x25	/* mcasp0_ahclkx.pr1_pru0_pru_r30_7, MODE6 | INPUT | PRU */
					
					0x1a8 0x25	/* mcasp0_axr1.pr1_pru0_pru_r30_6, MODE5 | OUTPUT | PRU */
					0x1a0 0x26	/* mcasp0_aclkr.pr1_pru0_pru_r30_4, MODE6 | INPUT | PRU */
					0x19c 0x25	/* mcasp0_ahclkr.pr1_pru0_pru_r30_3, MODE5 | OUTPUT | PRU */
					0x198 0x25	/* mcasp0_axr0.pr1_pru0_pru_r30_2, MODE5 | OUTPUT | PRU */
					0x194 0x25	/* mcasp0_fsx.pr1_pru0_pru_r30_1, MODE5 | OUTPUT | PRU */
					0x190 0x25	/* mcasp0_aclkx.pr1_pru0_pru_r30_0, MODE5 | OUTPUT | PRU */

//					0x084 0x25	/* gpmc_csn2.pr1_pru1_pru_r30_13, MODE5 | OUTPUT | PRU */
//					0x080 0x25	/* gpmc_csn1.pr1_pru1_pru_r30_12, MODE5 | OUTPUT | PRU */
//          removed due to emmc clash

					0x0e0 0x25	/* lcd_vsync.pr1_pru1_pru_r30_8, MODE5 | OUTPUT | PRU */
					0x0e8 0x25	/* lcd_pclk.pr1_pru1_pru_r30_10, MODE5 | OUTPUT | PRU */
					0x0e4 0x25	/* lcd_hsync.pr1_pru1_pru_r30_9, MODE5 | OUTPUT | PRU */
					0x0ec 0x25	/* lcd_ac_bias_en.pr1_pru1_pru_r30_11, MODE5 | OUTPUT | PRU */
					0x0bc 0x25	/* lcd_data7.pr1_pru1_pru_r30_7, MODE5 | OUTPUT | PRU */
					0x0b0 0x25	/* lcd_data4.pr1_pru1_pru_r30_4, MODE5 | OUTPUT | PRU */
					0x0b4 0x25	/* lcd_data5.pr1_pru1_pru_r30_5, MODE5 | OUTPUT | PRU */
					0x0ac 0x25	/* lcd_data3.pr1_pru1_pru_r30_3, MODE5 | OUTPUT | PRU */
					0x0a0 0x25	/* lcd_data0.pr1_pru1_pru_r30_0, MODE5 | OUTPUT | PRU */
					0x0a4 0x25	/* lcd_data1.pr1_pru1_pru_r30_1, MODE5 | OUTPUT | PRU */
					
				>;
			};
			
			pinctrl_eqep1: pinctrl_eqep0_pins {
                pinctrl-single,pins = <
                	0x164 0x03  /* ECAP PWM      P9->42 */
					0x0D0 0x32  /* eQEP1A_in     P8->35 */
					0x0D4 0x32  /* eQEP1B_in     P8->33 */
				>;
			};
			pinctrl_ehrpwm1: pinstrl_ehrpwm1_pins {
				pinctrl-single,pins = <
					0x048 0x26
				>;
				
			};
		};
	};

    /************************/
    /* PRUSS */
    /************************/

    fragment@1 {
        target = <&pruss>;
        __overlay__ {
            status = "okay";
			
			pinctrl-names = "default";
			pinctrl-0 = <&pru_pru_pins>;
        };
    };
    
    
    fragment@2 {
        target = <&epwmss1>;
        __overlay__ {
            status = "okay";
        };
    };
    
    fragment@3 {
    	target = <&eqep1>;
    	__overlay__ {
            pinctrl-names = "default";
            pinctrl-0 = <&pinctrl_eqep1>;
            
            count_mode = <0>;  /* 0 - Quadrature mode, normal 90 phase offset cha & chb.  1 - Direction mode.  cha input = clock, chb input = direction */
            swap_inputs = <0>; /* Are channel A and channel B swapped? (0 - no, 1 - yes) */
            invert_qa = <1>;   /* Should we invert the channel A input?  */
            invert_qb = <1>;   /* Should we invert the channel B input? I invert these because my encoder outputs drive transistors that pull down the pins */
            invert_qi = <0>;   /* Should we invert the index input? */
            invert_qs = <0>;   /* Should we invert the strobe input? */
            
	    status = "okay";
    	};
    };
    
    fragment@4 {
        target = <&ehrpwm1>;
        __overlay__ {
            status = "okay";
            pinctrl-names = "default";
            pinctrl-0 = <&pinctrl_ehrpwm1>; 
        };
    };
};


