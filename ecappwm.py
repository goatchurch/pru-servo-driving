from mmap import mmap
import time, struct

PWMSS_offset = 0x48302000
PWMSS_size = 0x4830225F - PWMSS_offset + 1

CM_PER_offset = 0x44E00000
CM_PER_size = 0x44E03FFF - CM_PER_offset + 1
CM_PER_EPWMSS1_CLKCTRL = 0xCC    # p1161 and p1198


fc = open("/dev/mem", "r+b" )
print("hi there")
memc = mmap(fc.fileno(), CM_PER_size, offset=CM_PER_offset)
print(memc)

print("clcktl",  struct.unpack("<L", memc[CM_PER_EPWMSS1_CLKCTRL:CM_PER_EPWMSS1_CLKCTRL+4]))
print("clcktl",  struct.unpack("<L", memc[CM_PER_EPWMSS1_CLKCTRL:CM_PER_EPWMSS1_CLKCTRL+4]))
memc[CM_PER_EPWMSS1_CLKCTRL:CM_PER_EPWMSS1_CLKCTRL+4] = struct.pack("<L", 0x0002)
print("clcktl",  struct.unpack("<L", memc[CM_PER_EPWMSS1_CLKCTRL:CM_PER_EPWMSS1_CLKCTRL+4]))


fp = open("/dev/mem", "r+b" )
print("hi thereq", PWMSS_size, PWMSS_offset)
memp = mmap(fp.fileno(), PWMSS_size, offset=PWMSS_offset)
print(memp)



# page 2383 of manual
ECCTL2 = 0x100 + 0x2A
CAP1 = 0x100 + 0x08
CAP2 = 0x100 + 0x0C


    # bit 4 TSCTRSTOP=1 makes TSCTR free running

def mempset4(offs, val):
    reg = struct.unpack("<L", memp[offs:offs+4])[0]
    vreg = val
    memp[offs:offs+4] = struct.pack("<L", vreg)
    lreg = struct.unpack("<L", memp[offs:offs+4])[0]
    print("Setting ", hex(offs), hex(reg), hex(vreg), hex(lreg))

def mempset2(offs, val):
    reg = struct.unpack("<L", memp[offs:offs+4])[0]
    vreg = (reg&0xFFFF0000) + val
    memp[offs:offs+4] = struct.pack("<L", vreg)
    lreg = struct.unpack("<L", memp[offs:offs+4])[0]
    print("Setting ", hex(offs), hex(vreg), hex(lreg))

memp.seek(CAP1)
#memp.write_byte(struct.pack("<b", 0xC0))

mempset4(ECCTL2, 0x02C0)  # CAP_APWM=1, SYNCO_SEL=(Disable sync out signal)

while True:
    mempset4(CAP1, 0x1010)   # APRD in PWM mode
    mempset4(CAP2, 0x0810)   # APRD in PWM mode (also.  should it be ACMP?)
    reg = struct.unpack("<L", memp[ECCTL2:ECCTL2+4])[0]
    #memp[ECCTL2:ECCTL2+4] = struct.pack("<L", reg | 0x0010)
    mempset2(ECCTL2, 0x02C0 | 0x0010)

    time.sleep(0.5)
    print("ping")
 
 
 
