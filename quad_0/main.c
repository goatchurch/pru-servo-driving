#include <stdint.h>
#include <pru_cfg.h>
//#include "resource_table_0.h"

volatile register uint32_t __R30;

/* Shared PID data structure - ensure both shared stucts match PRU 1 */
struct pid_data {
    /* PID tunings */
    int Kp_f, Ki_f, Kd_f;

    /* PID controls */
    int setpoint;
    int int_err;
    int input, output, last_output;
    int min_output, max_output;
};

/* Shared memory block struct */
struct shared_mem {
    volatile char init_flag;
    volatile unsigned int pwm_out;
    volatile int enc_rpm;
    volatile struct pid_data pid;
};

/* Output struct */
struct output_s {
	volatile unsigned int pwm;
	volatile int quad;
};
/* 'Float' conversion */
#define SHIFT    0x0E

/* Function prototypes */
void update_pid(volatile struct pid_data* pid);
void init_pid(volatile struct pid_data* pid);
void blip(void);

/* Mapping Constant table register to variable */
volatile near struct output_s output __attribute__((cregister("PRU_DMEM_0_1", near), peripheral));

/* Shared memory setup */
#pragma DATA_SECTION(share_buff, ".share_buff")
volatile far struct shared_mem share_buff;

#define LOOPS (*((volatile unsigned int *)0x4A301000))

/*
 * main.c
 */
void main(void) {
    LOOPS = 0;

    __R30 = 0x00;

    /* Allow for PRU core 1 to initialize */
    while (!share_buff.init_flag == 1);

    //blip();

    /* Initialize PIDs */
    init_pid(&share_buff.pid);

    /* Set default PID tunings */
    share_buff.pid.Kp_f    = 500;
    share_buff.pid.Ki_f    = 300;
    share_buff.pid.Kd_f    = 200;

    share_buff.pid.max_output = 4096;
    share_buff.pid.min_output = 0;

    share_buff.pid.setpoint = 0;

    /* Main loop */
	while(1) {
	    //update_pid(&share_buff.pid);
	    //if (share_buff.pid.input){
	    //share_buff.pid.output = 0x800;
	    	output.quad = share_buff.pid.input;
	    	output.pwm = share_buff.pid.output;
	    //}
	    LOOPS += 1;
	}
}

/*
 * update_pid
 */
void update_pid(volatile struct pid_data* pid) {
    unsigned int p_f, d_f;
    int output_f, output;

    /* Calculate error */
    int error = (pid->input - pid->setpoint);

    /* Calculate P term */
    p_f = pid->Kp_f * error;

    /* Integrate I term */
    pid->int_err += (pid->Ki_f * error) >> SHIFT;

    /* Calculate D term */
    d_f = pid->Kd_f * (pid->output - pid->last_output);

    /* Sum PID output */
    output_f = p_f + pid->int_err + d_f;
    output = output_f >> SHIFT;
    //output = p_f + d_f;

    /* Set output_f, check min/max output */
    if (output < pid->min_output) output = pid->min_output;
    if (output > pid->max_output) output = pid->max_output;

    pid->last_output = pid->output;
    pid->output = pid->max_output - output;
}

/*
 * init_pid
 */
void init_pid(volatile struct pid_data* pid) {
    pid->input = 0;
    pid->output = 0;
    pid->last_output = 0;

    pid->setpoint = 0;
    pid->int_err = 0;
}

void blip(void)
{
	__R30 &= 0xFFFF0000;
	__R30 ^= 1;
	__delay_cycles(100000000);
	__R30 ^= 1;
	__delay_cycles(100000000);
}
