
#include <stdio.h>

// echo BB-BONE-PRUCAPE > /sys/devices/bone_capemgr.9/slots
// gcc -Wall -Werror -o simplepid_arm -lpthread -lprussdrv prumusicchannels_arm.c

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdint.h>
#include <prussdrv.h>
#include <pruss_intc_mapping.h>

#define USE_PRU_0 1 
#define BIN_0 "./qepreader.bin"
#define USE_PRU_1 0           // PRU 1 not used at present.
#define BIN_1 "../test2.bin"
#define STILL 2048

/* Shared PID data structure - ensure stuct matches PRU */
struct pid_data {
    unsigned int notes[24];
    unsigned int stuff[100];
};

/*** 
 * pru_setup()
 * Initializes the PRU specified by PRU_NUM and sets up PRU_EVTOUT_0 handler.
 * Returns 0 on success, non-0 on error. 
***/
static int pru_setup(int pru_num) 
{
   int rtn;
   tpruss_intc_initdata intc = PRUSS_INTC_INITDATA;

   /* initialize PRU */
   if((rtn = prussdrv_init()) != 0) {
      fprintf(stderr, "prussdrv_init() failed\n");
      return rtn;
   }

   /* open the interrupt */
   if((rtn = prussdrv_open(PRU_EVTOUT_0)) != 0) {
      fprintf(stderr, "prussdrv_open() failed\n");
      return rtn;
   }

   /* initialize interrupt */
   if((rtn = prussdrv_pruintc_init(&intc)) != 0) {
      fprintf(stderr, "prussdrv_pruintc_init() failed\n");
      return rtn;
   }
   return rtn;
}

/*** pru_run()
 * Loads binary file and starts PRU running.
 * Returns 0 on success, non-0 on error. 
***/
static int pru_run(int pru_num, const char * path)
{  
   int rtn;
   /* load and run the PRU program */
   if((rtn = prussdrv_exec_program(pru_num, path)) < 0) 
      fprintf(stderr, "prussdrv_exec_program() failed\n");
   return rtn;
}

/*** pru_cleanup() -- halt PRU and release driver
 * Performs all necessary de-initialization tasks for the prussdrv library.
 * Returns 0 on success, non-0 on error.
***/
static int pru_cleanup(void) {
   int rtn = 0;

   /* clear the event (if asserted) */
   if(prussdrv_pru_clear_event(PRU_EVTOUT_0, PRU0_ARM_INTERRUPT)) {
      fprintf(stderr, "prussdrv_pru_clear_event() failed\n");
      rtn = -1;
   }

   /* halt and disable the PRU (if running) */
   if (USE_PRU_0){
      if((rtn = prussdrv_pru_disable(0)) != 0) {
         fprintf(stderr, "prussdrv_pru0_disable() failed\n");
         rtn = -1;
      }
   }
   if (USE_PRU_1){
      if((rtn = prussdrv_pru_disable(1)) != 0) {
         fprintf(stderr, "prussdrv_pru1_disable() failed\n");
         rtn = -1;
      }
   }
   /* release the PRU clocks and disable prussdrv module */
   if((rtn = prussdrv_exit()) != 0) {
      fprintf(stderr, "prussdrv_exit() failed\n");
      rtn = -1;
   }

   return rtn;
}

/***-------------------------- Main -----------------------------------***/

int main(int argc, char **argv) {
   
   int i, k;

   static void *pru_data_mem;
   static struct pid_data *pid;
   
   /* prussdrv_init() will segfault if called with EUID != 0 */ 
   if(geteuid()) {
      fprintf(stderr, "%s must be run as root to use prussdrv\n", argv[0]);
      return -1;
   }

   /* initialize the library, PRU and interrupt */
   if(USE_PRU_0)
      if(pru_setup(0) != 0) {
         pru_cleanup();
         return -1;
      }
   if(USE_PRU_1)
      if(pru_setup(1) != 0) {
         pru_cleanup();
         return -1;
      }
   
   /* setup pru memory mapping*/
   prussdrv_map_prumem (PRUSS0_PRU0_DATARAM, &pru_data_mem);
   pid = (struct pid_data*) pru_data_mem;
   
   for (i = 0; i < 24; i++)
         pid->notes[i] = 0; 
         
   
   /* start the pru(s) running */
   if(USE_PRU_0)
      if(pru_run(0, BIN_0) != 0) {
         pru_cleanup();
         return -1;
      }   
   if(USE_PRU_1)
      if(pru_run(1, BIN_1) != 0) {
         pru_cleanup();
         return -1;
      }

   pid->notes[0] = 0*1000;  // wavelength
   pid->notes[1] = 1000;  // amplitude
   pid->notes[2] = 0*3200;  // wavelength
   pid->notes[3] = 1000;  // amplitude

   pid->notes[4] = 600;  // wavelength
   pid->notes[5] = 1500;  // amplitude

/*
for (k = 0; k < 10; k++) {
for (i = 0; i < 20; i++) {
   pid->notes[4] = i*50+500;  // wavelength
   pid->notes[2] = (25-i)*50+200;  // wavelength
   usleep(100000); 
}
}
*/

for (k = 0; k < 50; k++) {
   pid->notes[5] = (k % 4 == 1 ? 1500 : 500);  // amplitude
for (i = 200; i < 2000; i += 4) {
   pid->notes[4] = i;  // wavelength
   usleep(2); 
}
   pid->notes[4] = 0; 
   usleep(100000); 
}


for (k = 0; k < 20; k++) {
for (i = 0; i < 101; i++)
   printf("%d ", pid->stuff[i]); 
printf("\n\n"); 
pid->stuff[0] = 0; 
usleep(1000000); 
   pid->notes[3] = 1000-pid->notes[3]; 
   pid->notes[2] -= 100; 
printf("%d\n", pid->notes[2]); 
   
}

   /* hold position */
   //while(1);
   
   /* clear the event, disable the PRU and let the library clean up */
   return pru_cleanup();
}

#include <stdlib.h>
#include <unistd.h>
#include <stdint.h>
#include <prussdrv.h>
#include <pruss_intc_mapping.h>

#define USE_PRU_0 1 
#define BIN_0 "./simplepid.bin"
#define USE_PRU_1 0           // PRU 1 not used at present.
#define BIN_1 "../test2.bin"
#define STILL 2048

/* Shared PID data structure - ensure stuct matches PRU */
struct pid_data {
   
   /* PID tunings */
   int Kp_f, Ki_f, Kd_f;

   /* PID controls */
   int setpoint;
   int p_f;
   int int_err;
   int d_f;
   int input, output, last_error;
   int min_output, max_output;
   int last_d_f;
};

/*** 
 * pru_setup()
 * Initializes the PRU specified by PRU_NUM and sets up PRU_EVTOUT_0 handler.
 * Returns 0 on success, non-0 on error. 
***/
static int pru_setup(int pru_num) 
{
   int rtn;
   tpruss_intc_initdata intc = PRUSS_INTC_INITDATA;

   /* initialize PRU */
   if((rtn = prussdrv_init()) != 0) {
      fprintf(stderr, "prussdrv_init() failed\n");
      return rtn;
   }

   /* open the interrupt */
   if((rtn = prussdrv_open(PRU_EVTOUT_0)) != 0) {
      fprintf(stderr, "prussdrv_open() failed\n");
      return rtn;
   }

   /* initialize interrupt */
   if((rtn = prussdrv_pruintc_init(&intc)) != 0) {
      fprintf(stderr, "prussdrv_pruintc_init() failed\n");
      return rtn;
   }
   return rtn;
}

/*** pru_run()
 * Loads binary file and starts PRU running.
 * Returns 0 on success, non-0 on error. 
***/
static int pru_run(int pru_num, const char * path)
{  
   int rtn;
   /* load and run the PRU program */
   if((rtn = prussdrv_exec_program(pru_num, path)) < 0) 
      fprintf(stderr, "prussdrv_exec_program() failed\n");
   return rtn;
}

/*** pru_cleanup() -- halt PRU and release driver
 * Performs all necessary de-initialization tasks for the prussdrv library.
 * Returns 0 on success, non-0 on error.
***/
static int pru_cleanup(void) {
   int rtn = 0;

   /* clear the event (if asserted) */
   if(prussdrv_pru_clear_event(PRU_EVTOUT_0, PRU0_ARM_INTERRUPT)) {
      fprintf(stderr, "prussdrv_pru_clear_event() failed\n");
      rtn = -1;
   }

   /* halt and disable the PRU (if running) */
   if (USE_PRU_0){
      if((rtn = prussdrv_pru_disable(0)) != 0) {
         fprintf(stderr, "prussdrv_pru0_disable() failed\n");
         rtn = -1;
      }
   }
   if (USE_PRU_1){
      if((rtn = prussdrv_pru_disable(1)) != 0) {
         fprintf(stderr, "prussdrv_pru1_disable() failed\n");
         rtn = -1;
      }
   }
   /* release the PRU clocks and disable prussdrv module */
   if((rtn = prussdrv_exit()) != 0) {
      fprintf(stderr, "prussdrv_exit() failed\n");
      rtn = -1;
   }

   return rtn;
}

/***-------------------------- Main -----------------------------------***/

int main(int argc, char **argv) {
   
   int i;
   FILE* fout;
   
   static void *pru_data_mem;
   static struct pid_data *pid;
   
   int nresults = 900;
   struct pid_data results[nresults];

   /* prussdrv_init() will segfault if called with EUID != 0 */ 
   if(geteuid()) {
      fprintf(stderr, "%s must be run as root to use prussdrv\n", argv[0]);
      return -1;
   }

   /* initialize the library, PRU and interrupt */
   if(USE_PRU_0)
      if(pru_setup(0) != 0) {
         pru_cleanup();
         return -1;
      }
   if(USE_PRU_1)
      if(pru_setup(1) != 0) {
         pru_cleanup();
         return -1;
      }
   
   /* setup pru memory mapping*/
   prussdrv_map_prumem (PRUSS0_PRU0_DATARAM, &pru_data_mem);
   pid = (struct pid_data*) pru_data_mem;
   
   /* Set default PID tunings */
   pid->Kp_f    = 50000;
   pid->Ki_f    = 4000;
   pid->Kd_f    = 5000;
   
   pid->max_output = 1536; 
   pid->min_output = -1536;
   
   /* start the pru(s) running */
   if(USE_PRU_0)
      if(pru_run(0, BIN_0) != 0) {
         pru_cleanup();
         return -1;
      }   
   if(USE_PRU_1)
      if(pru_run(1, BIN_1) != 0) {
         pru_cleanup();
         return -1;
      }

   /* read data from pru memory */
   for (i = 0; i < nresults; i++){
      
      /* impulse response */
      if (i == 300){
         pid->setpoint = 100;
         printf("high\n");
      }
      if (i == 600){
         pid->setpoint = 0;
         printf("low\n");
      }
      
      //printf("%d\n", pid->input);
      results[i] = *pid;
      usleep(1000); // 1ms
   }
   
   /* stop movement */
   //pid->output = STILL;
   
   /* logging */
   fout = fopen("output", "w");
   for (i=0; i<nresults; i++)
      fprintf(fout, "%d, %d, %d, %d, %d, %d, %d\n", 
               results[i].setpoint, 
               results[i].output, 
               results[i].input, 
               results[i].p_f,
               results[i].int_err,
               results[i].d_f,
               results[i].last_error);
   fclose(fout);
   
   /* hold position */
   //while(1);
   
   /* clear the event, disable the PRU and let the library clean up */
   return pru_cleanup();
}
