#!/usr/bin/env python
"""
Pru synth controller.
"""

# uses python-midi module :
# git clone https://github.com/vishnubob/python-midi.git
# needs swig package to install:
# apt-get install swig
# then:
# python setup.py install

# cape:
# echo BB-BONE-PRUCAPE > /sys/devices/bone_capemgr.9/slots

# pin:
# P9_42

import sys
import time
import midi
import midi.sequencer as sequencer
from midi.events import NoteOnEvent, NoteOffEvent
from math import pow
import pypruss

#--------PRU stuff:---------------#

pypruss.modprobe()
pypruss.init()
pypruss.open(0)
pypruss.pruintc_init()
pypruss.pru_write_memory(0, 0, [0, 0])
pypruss.exec_program(0, "./qepreader.bin")

#-------MIDI stuff:---------------#

# vmpk will be 128 0 when run for the first time
# set this as command line options with midiplay for playing files 
client = 128
port = 0

seq = sequencer.SequencerRead(sequencer_resolution=120)
seq.subscribe_port(client, port)
seq.start_sequencer()

#------Main loop------------------#            
while True:
  event = seq.event_read()
  if event is not None: # and event.channel == channel:
      print event
      if isinstance(event, NoteOnEvent):
        #------note to freq. formula:-------#
        # fn = fo * a^n
        # where:
        # fo = 440Hz (A5, midi 69)
        # n = steps from A5
        # a = 2^1/12
        freq = 440 * pow(1.059463094359,  event.data[0] - 69)
        total_period = 0.00004096
        period = 1/freq
        cycles = int(period/total_period)
        print freq, cycles
        pypruss.pru_write_memory(0, event.channel*2, [cycles*16, event.data[1]*10])
      elif isinstance(event, NoteOffEvent):
        pypruss.pru_write_memory(0, event.channel*2, [0, 0])
 
