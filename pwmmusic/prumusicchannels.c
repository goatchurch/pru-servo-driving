/*
* simplepid.c
*
* pru code for pru 0 in a simple pid example
* mainly taken from ti example
*/

#include <stdint.h>
#include <limits.h>
#include <pru_cfg.h>
#include <pru_ecap.h>
#include <sys_pwmss.h>

#define NOTECHANNELS 12
#define LOGDATA 100

/* shared pid structure */
struct pid_data {
    unsigned int notes[NOTECHANNELS*2];
    unsigned int logdata[LOGDATA];
};

volatile register uint32_t __R30;
volatile register uint32_t __R31;

/* Memory mapping */
volatile near struct pid_data pid __attribute__((cregister("PRU_DMEM_0_1", near), peripheral));

/* interupts */
#define PRU0_ARM_EVT        (19)
#define PRU0_ARM_TRIGGER    (__R31 = PRU0_ARM_EVT - 16 | (1 << 5)

/* Non-CT register defines */
#define CM_PER_EPWMSS1 (*((volatile unsigned int *)0x44E000CC))


int main(void)
{
	/* allow OCP master port access by the PRU so the PRU can read external memories */
	CT_CFG.SYSCFG_bit.STANDBY_INIT = 0;

	/* Configure GPI and GPO as Mode 0 (Direct Connect) */
	CT_CFG.GPCFG0 = 0x0000;

    /* Enable APWM mode and enable asynchronous operation; set polarity to active high */
    CT_ECAP.ECCTL2 = 0x02C0;

    /* Set number of clk cycles in a PWM period (APRD) */
    CT_ECAP.CAP1 = 4096;

    /* Enable ECAP PWM Freerun counter */
    CT_ECAP.ECCTL2 |= 0x0010;

    int i;
    long channelcounts[NOTECHANNELS];
    for (i = 0; i < NOTECHANNELS; i++) {
    	channelcounts[i] = 0;
    	pid.notes[i*2] = 0;
    	pid.notes[i*2+1] = 0;
    }
    for (i = 0; i < LOGDATA; i++)
    	pid.logdata[i] = 0;

    // derive the boundary cases from the period length
    int mid50 = CT_ECAP.CAP1/2;
    int guardlo = mid50/8;
    int maxamplitude = mid50 - guardlo;

	while(1){
		int waveamp = 0;
		for (i = 0; i < NOTECHANNELS; i++) {
			if ((pid.notes[i*2] == 0) || (pid.notes[i*2+1] == 0))
				continue;
			channelcounts[i] -= 16;
			if (channelcounts[i] < 0)
				channelcounts[i] += pid.notes[i*2];
			int amp = (pid.notes[i*2+1] * channelcounts[i]) / pid.notes[i*2] - pid.notes[i*2+1] / 2;
			waveamp += amp;
		}

		// intermittent logging of the calculations
		if (pid.logdata[0] < LOGDATA - 1) {
			pid.logdata[0]++;
			pid.logdata[pid.logdata[0]] = waveamp;
		}

		// attenuate to the maxamplitude asymptotically
		waveamp = waveamp*maxamplitude/(abs(waveamp) + maxamplitude);

		// bracket the amplitude (not necessary unless line above commented)
		if (waveamp < -maxamplitude)
			waveamp = -maxamplitude;
		if (waveamp > maxamplitude)
			waveamp = maxamplitude;

		waveamp += mid50;

		// wait till we are outside of the lower values of the clock
		while (CT_ECAP.TSCTR < guardlo)
			;
		// wait till the clock has ticked over (but hasn't counted above 100)
		while (!(CT_ECAP.TSCTR < guardlo))
			;

		// set the next duty cycle length
		CT_ECAP.CAP2 = waveamp;
	}
	return 0;
}



