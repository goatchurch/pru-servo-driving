from mmap import mmap
import time, struct

PWMSS_offset = 0x48302000
PWMSS_size = 0x4830225F - PWMSS_offset + 1

CM_PER_offset = 0x44E00000
CM_PER_size = 0x44E03FFF - CM_PER_offset + 1
CM_PER_EPWMSS1_CLKCTRL = 0xCC    # p1161 and p1198


fc = open("/dev/mem", "r+b" )
print("hi there")
memc = mmap(fc.fileno(), CM_PER_size, offset=CM_PER_offset)
print(memc)

print("clcktl",  struct.unpack("<L", memc[CM_PER_EPWMSS1_CLKCTRL:CM_PER_EPWMSS1_CLKCTRL+4]))
print("clcktl",  struct.unpack("<L", memc[CM_PER_EPWMSS1_CLKCTRL:CM_PER_EPWMSS1_CLKCTRL+4]))
memc[CM_PER_EPWMSS1_CLKCTRL:CM_PER_EPWMSS1_CLKCTRL+4] = struct.pack("<L", 0x0002)
print("clcktl",  struct.unpack("<L", memc[CM_PER_EPWMSS1_CLKCTRL:CM_PER_EPWMSS1_CLKCTRL+4]))


fp = open("/dev/mem", "r+b" )
print("hi thereq", PWMSS_size, PWMSS_offset)
memp = mmap(fp.fileno(), PWMSS_size, offset=PWMSS_offset)
print(memp)



# page 2383 of manual
ECCTL2 = 0x100 + 0x2A
CAP1 = 0x100 + 0x08
CAP2 = 0x100 + 0x0C

# p2321 of manual (these are 16 bit registers)
TBCTL = 0x200 + 0x00
CMPCTL = 0x200 + 0x0E
CMPA = 0x200 + 0x12
TBCNT = 0x200 + 0x08
TBPRD = 0x200 + 0x0A
AQCTLA = 0x200 + 0x16


def mempset2(offs, val):
    reg = struct.unpack("<L", memp[offs:offs+4])[0]
    vreg = (reg&0xFFFF0000) + val
    memp[offs:offs+4] = struct.pack("<L", vreg)
    lreg = struct.unpack("<L", memp[offs:offs+4])[0]
    print("Setting ", hex(offs), hex(vreg), hex(lreg))
    
mempset2(TBCTL, 0xDF04)
#mempset2(TBPRD, 65104)
#mempset2(CMPA, 13020)

mempset2(CMPCTL, 0x0010)
#mempset2(AQCTLA, 0x0045)
mempset2(CMPA, 0x20)


while True:
    res = [ ]
    for i in [0x200, 0x208, 0x20A, 0x20E, 0x212, 0x216 ]:
        res.append("%s:%s" % (hex(i), hex(struct.unpack("<L", memp[i:i+4])[0]&0xFFFF)))
    print(" ".join(res))
    time.sleep(1.1)
    
    


memp[ECCTL2:ECCTL2+4] = struct.pack("<L", 0x02C0)  # CAP_APWM=1, SYNCO_SEL=(Disable sync out signal)
    # bit 4 TSCTRSTOP=1 makes TSCTR free running

while True:
    memp[CAP1:CAP1+4] = struct.pack("<L", 0x1000)   # APRD in PWM mode
    memp[CAP2:CAP2+4] = struct.pack("<L", 0x0800)   # APRD in PWM mode (also.  should it be ACMP?)
    reg = struct.unpack("<L", memp[ECCTL2:ECCTL2+4])[0]
    memp[ECCTL2:ECCTL2+4] = struct.pack("<L", reg | 0x0010)
    
    time.sleep(0.5)
    print("ping")
 
 
 
