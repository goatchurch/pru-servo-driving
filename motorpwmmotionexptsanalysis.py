fname = "/home/goatchurch/geom3d/pru-servo-driving/motorcurves.txt"
ls = open(fname).readlines()

lss = [ ]
for l in ls:
    if not l.strip():
        lss.append([])
    elif not lss[-1]:
        lss[-1].append(int(l.strip()))
    else:
        usec, qpos = list(map(int, l.split()))[1:]
        lss[-1].append((usec + (1000000 if usec<0 else 0), qpos))
    
lsss
    
sendactivity("clearalltriangles")

ufac = 0.0001
vfac = 0.006
for lsss in lss[:]:
    #vfac = 20/lsss[-1]
    sendactivity(contours=[[(usec*ufac, v*vfac)  for usec, v in lsss[1:]]], materialnumber=(3 if lsss[-1][1] < 0 else 0))
    #sendactivity(contours=[[(usec*ufac, v*vfac-1), (usec*ufac, v*vfac+1)]  for usec, v in [lsss[11], lsss[201], lsss[401]]], materialnumber=2)
    sendactivity(points=[(usec*ufac, v*vfac)  for usec, v in [lsss[11], lsss[201], lsss[401]]], materialnumber=2)

150/500*30

print([ lss[i][500][0]*ufac  for i in range(len(lss)) ])


sendactivity(points=[(usec*ufac, v*vfac)  for usec, v in [lsss[11], lsss[201], lsss[401]]], materialnumber=2)

lsss[0]
lsss[11]
lsss[201]

import math, scipy.optimize
math.exp
def fun
scipy.optimize.minimize

0.001**2

def fun(X, tvseq):
    fac, lam = X
    return sum((v-fac*math.exp(-lam*(t-tvseq[0][0])))**2  for t, v in tvseq)

voltgraph = [ ]
for lsss in lss[:]:
    tvseq = [ ]
    #for i in range(15, 195):
    volts = (lsss[0]-500)/500*30
    for i in range(405, 595):
        v = 1e6*(lsss[i+dd][1] - lsss[i-dd][1])/(lsss[i+dd][0] - lsss[i-dd][0])/400
        tvseq.append((lsss[i][0]*1e-6, v/400))
    while tvseq and abs(tvseq[-1][1])<0.0005:
        tvseq.pop()
    if len(tvseq)>10:
        res = scipy.optimize.minimize(fun=fun, x0=(tvseq[0][1],1), args=(tvseq,))
        sendactivity(contours=[[(t*10, v)  for t, v in tvseq]], materialnumber=1)
        print(res["x"])
        fac, lam = res["x"]
        sendactivity(contours=[[(t*10, fac*math.exp(-lam*(t-tvseq[0][0])))  for t, v in tvseq]], materialnumber=2)
    voltgraph.append((volts, res["x"][1]))
        
voltgraph.sort()
sendactivity(contours=[[(v, e*10)  for v, e in voltgraph]])

", ".join(["%d"%int(e)  for v, e in voltgraph])

sum(e  for v, e in voltgraph[-20:])/20



def fun(X, tvseq):
    fac, lam, lim = X
    return sum((v-(lim+fac*math.exp(-lam*(t-tvseq[0][0]))))**2  for t, v in tvseq)


voltgraph = [ ]
voltspeed = [ ]
for lsss in lss[:]:
    tvseq = [ ]
    volts = (lsss[0]-500)/500*30
    for i in range(10, 195):
        v = 1e6*(lsss[i+dd][1] - lsss[i-dd][1])/(lsss[i+dd][0] - lsss[i-dd][0])/400
        tvseq.append((lsss[i][0]*1e-6, v/400))   # erroneous extra div by 400
    while tvseq and abs(tvseq[0][1])<0.0025:
        tvseq.pop(0)
    del tvseq[:4]
    if len(tvseq)>10:
        res = scipy.optimize.minimize(fun=fun, x0=(-tvseq[-1][1], 1, tvseq[-1][1]), args=(tvseq,))
        sendactivity(contours=[[(t*10, v)  for t, v in tvseq]], materialnumber=1)
        print(res["x"])
        fac, lam, lim = res["x"]
        sendactivity(contours=[[(t*10, lim+fac*math.exp(-lam*(t-tvseq[0][0])))  for t, v in tvseq]], materialnumber=2)
    voltgraph.append((volts, res["x"][1]))
    voltspeed.append((volts, res["x"][2]))
    

", ".join(["%d"%int(e*10)  for v, e in voltgraph])
voltspeed.sort()
sendactivity(points=[(v*0.01,l)  for v, l in voltspeed])
sendactivity(contours=[[(v*0.01,l)  for v, l in voltspeed]])

voltspeed[-1][1]
voltspeed[0][1]*400/voltspeed[0][0]






def fun(X, tvseq):
    fac, lam, lim = X
    return sum((v-(lim+fac*math.exp(-lam*(t-tvseq[0][0]))))**2  for t, v in tvseq)

voltgraph = [ ]
voltspeed = [ ]
for lsss in lss[:]:
    tvseq = [ ]
    volts = (lsss[0]-500)/500*30
    for i in range(210, 390):
        v = 1e6*(lsss[i+dd][1] - lsss[i-dd][1])/(lsss[i+dd][0] - lsss[i-dd][0])/400
        tvseq.append((lsss[i][0]*1e-6, v/400))   # erroneous extra div by 400
    del tvseq[:5]
    if len(tvseq)>10:
        res = scipy.optimize.minimize(fun=fun, x0=(-tvseq[-1][1], 1, tvseq[-1][1]), args=(tvseq,))
        sendactivity(contours=[[(t*10, v)  for t, v in tvseq]], materialnumber=1)
        print(res["x"])
        fac, lam, lim = res["x"]
        sendactivity(contours=[[(t*10, lim+fac*math.exp(-lam*(t-tvseq[0][0])))  for t, v in tvseq]], materialnumber=2)
    voltgraph.append((volts, res["x"][1]))
    voltspeed.append((volts, res["x"][2]))

", ".join(["%d"%int(e)  for v, e in voltgraph])
voltspeed.sort()
sendactivity(points=[(v*0.01,l)  for v, l in voltspeed])
sendactivity(contours=[[(v*0.01,l)  for v, l in voltspeed]])
