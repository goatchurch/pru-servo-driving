
/* user space side program for quadrature reader*/

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <unistd.h>

int main(int argc, char **argv)
{
    long GPIO1_offset = 0x4804c000;
    long GPIO_SIZE = 0x2000; 
    long USR1 = 1<<28; 
    long GPIO_OUTPUTENABLE = 0x134; 
    long GPIO_SETDATAOUT = 0x194; 
    long GPIO_CLEARDATAOUT = 0x190; 
    long i = 0; 
    long j = 0; 
    int fd; 
    void* gpioaddr; 
    unsigned int *outputenable_reg; 
    unsigned int *setdataout_reg; 
    unsigned int *cleardataout_reg; 
    
    printf("hi there\n\n");
    fd = open("/dev/mem", O_RDWR);
    gpioaddr = mmap(0, GPIO_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fd, GPIO1_offset);
    outputenable_reg = gpioaddr + GPIO_OUTPUTENABLE;
    setdataout_reg = gpioaddr + GPIO_SETDATAOUT; 
    cleardataout_reg = gpioaddr + GPIO_CLEARDATAOUT; 
    close(fd); 
    *outputenable_reg = *outputenable_reg & ~USR1; 
    for (i = 0; i < 200; i++) {
        fprintf(stderr, "ping %d\n", i);
        *setdataout_reg = USR1; 
        usleep(20*1000); 
        *cleardataout_reg = USR1; 
        usleep(100*1000); 
    }
}
