from mmap import mmap
import time, struct

# good enough
# echo bone_eqep1 > /sys/devices/bone_capemgr*/slots
# to see all capes: ls /lib/firmware

# given in p182 table 2-3 of the AM335x Sitara processor reference manual 
EQEP1_offset = 0x48302180
EQEP1_size = 0x483021FF - EQEP1_offset

PWMSS_offset = 0x48302000
PWMSS_size = 0x4830225F - PWMSS_offset + 1

# requires echo BB-BONE-PRUCAPE > /sys/devices/bone_capemgr.9/slots
# (need to work out what exactly is necessary in the above DTS)

fp = open("/dev/mem", "r+b" )
print("hi thereq", PWMSS_size, PWMSS_offset)
memp = mmap(fp.fileno(), PWMSS_size, offset=PWMSS_offset)
print(memp)

QPOSCNT = 0x180 + 0x00
QCAPCTL = 0x180 + 0x2C

memp[QCAPCTL:QCAPCTL+4] = struct.pack("<L", 0x8000)

while(True):
    reg = struct.unpack("<L", memp[QPOSCNT:QPOSCNT+4])[0]
    print(reg)
    time.sleep(0.1)

# echo bone_pwm_P9_42 > /sys/devices/bone_capemgr*/slots
# echo cape-universaln > /sys/devices/bone_capemgr.*/slots
# vim BB-BONE-PRUCAPE-00A0.dts
# dtc -O dtb -o BB-BONE-PRUCAPE-00A0.dtbo -b 0 -@ BB-BONE-PRUCAPE-00A0.dts
# echo BB-BONE-PRUCAPE > /sys/devices/bone_capemgr.9/slots 

# or
# echo bone_eqep1 >  /sys/devices/bone_capemgr*/slots


