/***************************************************************************************
 * MAIN.C
 *
 * Description: main source file for PRU development
 *
 * Rafael de Souza
 * (C) 2015 Texas Instruments, Inc
 * 
 * Built with Code Composer Studio v6
 **************************************************************************************/

#include <stdint.h>
#include <limits.h>
#include <pru_cfg.h>
#include <pru_ecap.h>
#include <sys_pwmss.h>

//#include "resource_table_empty.h"

/* Shared PID data structure - ensure both shared stucts match PRU 0 */
struct pid_data {
    /* PID tunings */
    int Kp_f, Ki_f, Kd_f;

    /* PID controls */
    int setpoint;
    int int_err;
    int input, output, last_output;
    int min_output, max_output;
};

/* Shared memory block struct */
struct shared_mem {
    volatile char init_flag;
    volatile unsigned int pwm_out;
    volatile int enc_rpm;
    volatile struct pid_data pid;
};



/* Shared memory setup */
#pragma DATA_SECTION(share_buff, ".share_buff")
volatile far struct shared_mem share_buff;
//volatile near int share_buff;

/* Mapping Constant table register to variable */
//volatile near int share_buff __attribute__((cregister("PRU_SHAREDMEM", near), peripheral));

/* PRU GPIO Registers */
volatile register uint32_t __R30;
volatile register uint32_t __R31;

/* Interupts */
#define PRU0_ARM_EVT (19)
#define PRU0_ARM_TRIGGER (__R31 = PRU0_ARM_EVT - 16 | (1 << 5))

/* Non-CT register defines */
#define CM_PER_EPWMSS1 (*((volatile unsigned int *)0x44E000CC))

/* PWM generation definitions*/
#define PERIOD_CYCLES       0x1000

/* Encoder definitions */
#define TICKS_PER_REV       24
#define SAMPLES_PER_SEC     12
#define SEC_PER_MIN         60


/* Prototypes */
//void init_pwm();
void init_eqep();
int get_enc_rpm();
void blip();

int main(void)
{
	/* Your code goes here */
	int rpm;

	/* allow OCP master port access by the PRU so the PRU can read external memories */
	CT_CFG.SYSCFG_bit.STANDBY_INIT = 0;
	/* Configure GPI and GPO as Mode 0 (Direct Connect) */
	CT_CFG.GPCFG0 = 0x0000;

	blip();

	init_pwm();
	init_eqep();

    /* Set init flag to signal PID to continue initializing */
    share_buff.init_flag = 1;

	while (1) {

	        /* Write PWM speed (ACMP) */
			//value = share_buff.pid.output + 2048;
			//value = value < 0 ? 0 : value;
			//value = value > 4096 ? 4096 : value;
			CT_ECAP.CAP2_bit.CAP2 = share_buff.pid.output;


	        /* Save write cycles by waiting until unit time event */
	        if (PWMSS1.EQEP_QFLG & 0x0800) {
	            PWMSS1.EQEP_QCLR |= 0x0800;
	            share_buff.pid.input= PWMSS1.EQEP_QPOSLAT;
	            //share_buff.pid.input = get_enc_rpm();
	            PRU0_ARM_TRIGGER;

	            //share_buff.pid.input = 0;
	        }
	}

	__halt();
	
	/* Should never return */
	return 0;
}

/*
 * Init eQEP
 */
void init_eqep() {
    /* Set RPM memory to 0 */
    share_buff.enc_rpm = 0;

    /* Enable PWMSS1 clock signal generation */
    while (!(CM_PER_EPWMSS1 & 0x2))
        CM_PER_EPWMSS1 |= 0x2;

    /* Set to defaults in quadriture mode */
    PWMSS1.EQEP_QDECCTL = 0x00;

    /* Enable unit timer
     * Enable capture latch on unit time out
     * Enable quadrature position counter
     * Enable software loading of position counter
     * Reset position counter on unit time event to gauge RPM
     */
    PWMSS1.EQEP_QEPCTL = 0x308E;

    /* Set prescalers for EQEP Capture timer and UPEVNT */
    /* Note: EQEP Capture unit must be disabled before changing prescalar */
    PWMSS1.EQEP_QCAPCTL = 0x0073;

    /* Enable EQEP Capture */
    PWMSS1.EQEP_QCAPCTL |= 0x8000;

    /* Enable unit time out interupt */
    PWMSS1.EQEP_QEINT |= 0x0800;

    /* Clear encoder count */
    PWMSS1.EQEP_QPOSCNT_bit.QPOSCNT = 0x00000000;

    /* Set max encoder count */
    PWMSS1.EQEP_QPOSMAX_bit.QPOSMAX = UINT_MAX;

    /* Clear timer */
    PWMSS1.EQEP_QUTMR_bit.QUTMR = 0x00000000;

    /* Set unit timer period count */
    /*  QUPRD = Period * 100MHz */
    PWMSS1.EQEP_QUPRD_bit.QUPRD = 0x007FFFFF; // (~1/12s) @ 100MHz

    /* Clear all interrupt bits */
    PWMSS1.EQEP_QCLR = 0xFFFF;
}

int get_enc_rpm() {
    int rpm = 0;

    /* Check for overrun/overflow errors, flash LED and show RPM as 0 */
    if (PWMSS1.EQEP_QEPSTS &= 0x0C) {
        PWMSS1.EQEP_QEPSTS |= 0x0C;
        __R30 = 0xFF;
        rpm = 0;
    } else {
        __R30 = 0x00;
        rpm = (PWMSS1.EQEP_QPOSLAT * SAMPLES_PER_SEC * SEC_PER_MIN) / TICKS_PER_REV;
    }

    return rpm;
}

void init_pwm() {
    /* Set PWM memory to 0 */
    share_buff.pwm_out = 0;

    /* Enable APWM mode and enable asynchronous operation; set polarity to active high */
    CT_ECAP.ECCTL2 = 0x02C0;

    /* Set number of clk cycles in a PWM period (APRD) */
    CT_ECAP.CAP1 = PERIOD_CYCLES;

    /* Enable ECAP PWM Freerun counter */
    CT_ECAP.ECCTL2 |= 0x0010;
}

void blip(void)
{
	__R30 &= 0xFFFF0000;
	__R30 ^= 1;
	__delay_cycles(100000000);
	__R30 ^= 1;
	__delay_cycles(100000000);
}

